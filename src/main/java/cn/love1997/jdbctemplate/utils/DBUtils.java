package cn.love1997.jdbctemplate.utils;

import cn.love1997.jdbctemplate.App;
import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import javax.sql.RowSet;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DBUtils {

    private static DataSource dataSource;

    static {
        // 配置文件
        try {
            InputStream is = App.class.getClassLoader().getResourceAsStream("druid.properties");
            Properties properties = new Properties();
            properties.load(is);
            dataSource = DruidDataSourceFactory.createDataSource(properties);
        } catch (IllegalArgumentException e) {
            System.out.println("文件有误");
        } catch (IOException e) {
            System.out.println("打开配置文件失败");
        } catch (Exception e) {
            System.out.println("德鲁伊数据源创建失败");
        }
    }

    /**
     * 获取链接
     *
     * @return 数据库链接
     */
    public static Connection getConnection() {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    /**
     * 资源释放
     */
    public static void release(Connection connection, Statement statement, RowSet rowSet) {
        try {
            release(connection, statement, null, rowSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void release(Connection connection, Statement statement, ResultSet resultSet) {
        try {
            release(connection, statement, resultSet, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void release(Connection connection, Statement statement, ResultSet resultSet, RowSet rowSet) throws SQLException {
        if (rowSet != null) {
            rowSet.close();
        }
        if (resultSet != null) {
            resultSet.close();
        }
        if (statement != null) {
            statement.close();
        }
        if (connection != null) {
            connection.close();
        }
    }

}
