package cn.love1997.jdbctemplate.template;

import java.util.List;

public abstract class JdbcTemplate<T> {

    protected Class<T> entityClass;

    protected JdbcTemplate(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public abstract List<T> query(String sql, Object... args);

}
