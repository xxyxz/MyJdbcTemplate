package cn.love1997.jdbctemplate.template;

import cn.love1997.jdbctemplate.utils.DBUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.*;


public class QueryTemplate<T> extends JdbcTemplate<T> {

    public QueryTemplate(Class<T> entityClass) {
        super(entityClass);
    }

    @Override
    public List<T> query(String sql, Object... args) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<T> result = new ArrayList<>();
        try {
            connection = DBUtils.getConnection();
            preparedStatement = connection.prepareStatement(sql);

            // 设置参数
            for (int i = 1; i <= args.length; i++) {
                setValue(i, args[i - 1], preparedStatement);
            }

            resultSet = preparedStatement.executeQuery();
            Map<String, String> metaMap = getMetaMap(preparedStatement);

            while (resultSet.next()) {
                T entity = getEntity(preparedStatement, resultSet);
                result.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DBUtils.release(connection, preparedStatement, resultSet);
        }
        return result;
    }

    /**
     * 获得具体的对象，并赋值
     */
    private T getEntity(PreparedStatement preparedStatement, ResultSet resultSet) {
        Map<String, String> metaMap = getMetaMap(preparedStatement);
        try {
            T object = entityClass.newInstance();
            Set<String> keySet = metaMap.keySet();
            for (String key : keySet) {
                Object value = getValue(key, metaMap.get(key), resultSet);
                // 获取属性
                Field field = entityClass.getDeclaredField(key);
                Method declaredMethod = entityClass.getMethod(constructSetMethod(key), field.getType());
                declaredMethod.invoke(object, value);
            }
            return object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获得数据库中 字段名称与数据类型的映射
     * @return Map
     */
    private static Map<String, String> getMetaMap(PreparedStatement preparedStatement) {
        Map<String, String> metaMap = new HashMap<>();
        try {
            ResultSetMetaData metaData = preparedStatement.getMetaData();
            // 遍历元素
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                String columnName = metaData.getColumnName(i);
                String columnTypeName = metaData.getColumnTypeName(i);
                metaMap.put(columnName, columnTypeName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return metaMap;
    }

    /**
     * 根据具体字段获得其属性赋值方法
     * @param key 成员名称
     * @return 方法名称
     */
    private static String constructSetMethod(String key) {
        String substring = key.substring(0, 1).toUpperCase() + key.substring(1);
        return "set" + substring;
    }

    /**
     * 预编译statement赋值
     * @param index 参数占位符 索引
     * @param value 具体值
     */
    private static void setValue(int index, Object value, PreparedStatement preparedStatement) {
        // 获得类型
        String typeName = value.getClass().getTypeName();
        try {
            switch (typeName) {
                case "java.lang.Long":
                    preparedStatement.setLong(index, Long.parseLong(value.toString()));
                    break;
                case "java.lang.String":
                    preparedStatement.setString(index, value.toString());
                    break;
                case "java.lang.Integer":
                    preparedStatement.setInt(index, Integer.parseInt(value.toString()));
                    break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * 从ResultSet中获取对应的值
     * @param key 字段
     * @param typeName 返回数据类型
     * @param resultSet 结果集
     * @return 值
     */
    private static Object getValue(String key, String typeName, ResultSet resultSet) {
        Object value = null;
        try {
            switch (typeName) {
                case "BIGINT":
                    value = resultSet.getLong(key);
                    break;
                case "INT":
                    value = resultSet.getInt(key);
                    break;
                case "VARCHAR":
                    value = resultSet.getString(key);
                    break;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return value;
    }


}
