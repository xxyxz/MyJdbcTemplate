package cn.love1997.jdbctemplate;


import cn.love1997.jdbctemplate.domain.User;
import cn.love1997.jdbctemplate.template.JdbcTemplate;
import cn.love1997.jdbctemplate.template.QueryTemplate;

import java.util.List;

public class App {

    public static void main(String[] args) {

        JdbcTemplate<User> jdbcTemplate= new QueryTemplate<>(User.class);
        // 定义sql语句
        String sql = "select * from user where age > 20";
        // 执行
        List<User> users = jdbcTemplate.query(sql);
        for (User user : users) {
            System.out.println(user);
        }
    }


}
